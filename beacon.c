/******************************************************************************
 *    Copyright (c) 2019 MapKey s.r.o.
 *
 * Zde se nastavuje advertisement
 *
 ******************************************************************************/

#include <gatt.h>           /* GATT application interface */
#include <buf_utils.h>      /* Buffer functions */
#include <mem.h>            /* Memory routines */
#include <gatt_prim.h>
#include <gatt_uuid.h>
#include <ls_app_if.h>
#include <gap_app_if.h>
#include "beacon.h"

/* Jak casto vysilam advertisement */
#define ADV_INTERVAL_MIN (4500 * MILLISECOND)
#define ADV_INTERVAL_MAX (5500 * MILLISECOND)


/* TX Power - hodnota, ktera se posila v advertisementu */
/* This is at 0 meters by spec (If 1m is required, subtract 41dBm from each) */
#define ADV_TX_POWER_FOR_NEG_18   (-22)
#define ADV_TX_POWER_FOR_NEG_10   (-14)
#define ADV_TX_POWER_FOR_NEG_2    (-6)
#define ADV_TX_POWER_FOR_POS_6    (2)

/* TX Power - hodnota, ktera se nastavuje v CSR */
/* Actual Power (dBm) vs value in register */
#define RADIO_TX_POWER_NEG_18 (0)
#define RADIO_TX_POWER_NEG_14 (1)
#define RADIO_TX_POWER_NEG_10 (2) 
#define RADIO_TX_POWER_NEG_6  (3)
#define RADIO_TX_POWER_NEG_2  (4)
#define RADIO_TX_POWER_POS_2  (5)
#define RADIO_TX_POWER_POS_6  (6)
#define RADIO_TX_POWER_POS_8  (7)

#define RADIO_TX_POWER_CONFIG RADIO_TX_POWER_NEG_2
#define ADV_TX_POWER_CONFIG ADV_TX_POWER_FOR_NEG_2

#define AD_TYPE_BATTERY_VOLTAGE 0xFE

/* Advertisement data, ktera posilam */
uint8 device_name[10 + 1] = {
    AD_TYPE_LOCAL_NAME_COMPLETE, 
    'C', 'S', 'R', ' ', 'B', 'E', 'A', 'C', 'O', 'N'
};

uint8 device_tx_power[2] = {
    AD_TYPE_TX_POWER,
    ADV_TX_POWER_CONFIG
};

uint8 device_appearance[3] = {
    AD_TYPE_APPEARANCE,
    0x40, 0x04   // 0x440 = 1088 = Running walking sensor
};

uint8 device_battery[3] = {
    AD_TYPE_BATTERY_VOLTAGE,
    0x34, 0x12
};

void BeaconStart(void)
{
    /* Stop broadcasting */
    LsStartStopAdvertise(FALSE, whitelist_disabled, ls_addr_type_public);
    
    /* set the GAP Broadcaster role */
    GapSetMode(gap_role_broadcaster,
               gap_mode_discover_no,
               gap_mode_connect_no,
               gap_mode_bond_no,
               gap_mode_security_none);
        
    /* clear the existing advertisement and scan response data */
    LsStoreAdvScanData(0, NULL, ad_src_advertise);
    LsStoreAdvScanData(0, NULL, ad_src_scan_rsp);
    
    /* add own advertisement data */    
    LsStoreAdvScanData(sizeof(device_name), device_name, ad_src_advertise);
    
    /* Update the radio tx power level here */
    LsSetTransmitPowerLevel(RADIO_TX_POWER_CONFIG); 

    /* Update the adv tx power level here */
    LsStoreAdvScanData(sizeof(device_tx_power), device_tx_power, ad_src_advertise);

    /* Add device appearance to the advertisements */
    LsStoreAdvScanData(sizeof(device_appearance), device_appearance, ad_src_advertise);
    
    /* Add device battery voltage to the advertisements */
    LsStoreAdvScanData(sizeof(device_battery), device_battery, ad_src_advertise);
    
    /* set the advertisement interval */
    GapSetAdvInterval(ADV_INTERVAL_MIN, ADV_INTERVAL_MAX);
    
    /* Start advertising */
    LsStartStopAdvertise(TRUE, whitelist_disabled, ls_addr_type_public);
}


void SetPressCount(uint32 count)
{
    int8 i;
    for(i = (sizeof(device_name)-1); i >= 1; i--)
    {
        device_name[i] = (count % 10) + '0';
        count /= 10;
    }
}

void SetBatteryVoltage(uint16 batteryVoltage)
{
    device_battery[1] = (batteryVoltage >> 8) & 0xff; 
    device_battery[2] = batteryVoltage & 0xff; 
}