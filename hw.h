/******************************************************************************
 *    Copyright (c) 2019 MapKey s.r.o.
 *
 *
 ******************************************************************************/

#ifndef __HW_H__
#define __HW_H__


/* Convert a PIO number into a bit mask */
#define PIO_BIT_MASK(pio)       (0x01UL << (pio))

/* PIO direction */
#define PIO_DIRECTION_INPUT     (FALSE)
#define PIO_DIRECTION_OUTPUT    (TRUE)

/* PIO state */
#define PIO_STATE_HIGH          (TRUE)
#define PIO_STATE_LOW           (FALSE)

/* Dull on. off and hold times */
#define DULL_BUZZ_ON_TIME                               (2)    /* 60us */
#define DULL_BUZZ_OFF_TIME                              (15)   /* 450us */
#define DULL_BUZZ_HOLD_TIME                             (0)
/* The index (0-3) of the PWM unit to be configured */
#define BUZZER_PWM_INDEX_0                              (0)

/* Bright on, off and hold times */
#define BRIGHT_BUZZ_ON_TIME                             (2)    /* 60us */
#define BRIGHT_BUZZ_OFF_TIME                            (15)   /* 450us */
#define BRIGHT_BUZZ_HOLD_TIME                           (0)    /* 0us */

#define BUZZ_RAMP_RATE                                  (0xFF)
#define BUTTON_PIO                  (11)
#define LED1_PIO                     (3)
#define LED2_PIO                     (4)
#define BUZZER_PIO                  (10)

#define BUTTON_PIO_MASK             (PIO_BIT_MASK(BUTTON_PIO))
#define LED1_PIO_MASK               (PIO_BIT_MASK(LED1_PIO))
#define LED2_PIO_MASK               (PIO_BIT_MASK(LED2_PIO))
#define BUZZER_PIO_MASK             (PIO_BIT_MASK(BUZZER_PIO))

void InitHardware(void);
void Led1On(void);
void Led1Off(void);
void Led2On(void);
void Led2Off(void);
void BuzzerOn(void);
void BuzzerOff(void);

#endif /* __HW_H__ */
