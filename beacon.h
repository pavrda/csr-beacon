/******************************************************************************
 *    Copyright (c) 2019 MapKey s.r.o.
 *
 *
 ******************************************************************************/

#ifndef __BEACON_H__
#define __BEACON_H__


/* Start or stop beaconing */
void BeaconStart(void);

void SetPressCount(uint32 count);
void SetBatteryVoltage(uint16 batteryVoltage);

#endif /* __BEACON_H__ */
