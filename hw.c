/******************************************************************************
 *    Copyright (c) 2019 MapKey s.r.o.
 *
 * Zde se nastavuje tlacitko, LEDky a buzzer
 *
 ******************************************************************************/

#include <pio.h>            /* PIO configuration and control functions */
#include "hw.h"

void InitHardware(void)
{
    // BUTTON
    PioSetModes(BUTTON_PIO_MASK, pio_mode_user);
    PioSetDir(BUTTON_PIO, PIO_DIRECTION_INPUT);
    PioSetPullModes(BUTTON_PIO_MASK, pio_mode_strong_pull_up);
    PioSetEventMask(BUTTON_PIO_MASK, pio_event_mode_both);

    // LED1
    PioSetModes(LED1_PIO_MASK, pio_mode_user);
    PioSetDir(LED1_PIO, PIO_DIRECTION_OUTPUT);
    PioSet(LED1_PIO, FALSE);

    // LED2
    PioSetModes(LED2_PIO_MASK, pio_mode_user);
    PioSetDir(LED2_PIO, PIO_DIRECTION_OUTPUT);
    PioSet(LED2_PIO, FALSE);

    // BUZZER
    PioSetModes(PIO_BIT_MASK(BUZZER_PIO), pio_mode_pwm0);
    PioConfigPWM(BUZZER_PWM_INDEX_0, pio_pwm_mode_push_pull, DULL_BUZZ_ON_TIME,
                 DULL_BUZZ_OFF_TIME, DULL_BUZZ_HOLD_TIME, BRIGHT_BUZZ_ON_TIME,
                 BRIGHT_BUZZ_OFF_TIME, BRIGHT_BUZZ_HOLD_TIME, BUZZ_RAMP_RATE);
    PioEnablePWM(BUZZER_PWM_INDEX_0, FALSE);
    
    /* Save power by changing the I2C pull mode to pull down.*/
    PioSetI2CPullMode(pio_i2c_pull_mode_strong_pull_down);
}

void Led1On()
{
    PioSet(LED1_PIO, TRUE);
}

void Led1Off()
{
    PioSet(LED1_PIO, FALSE);
}

void Led2On()
{
    PioSet(LED2_PIO, TRUE);
}

void Led2Off()
{
    PioSet(LED2_PIO, FALSE);
}

void BuzzerOn()
{
    PioEnablePWM(BUZZER_PWM_INDEX_0, TRUE);
}

void BuzzerOff()
{
    PioEnablePWM(BUZZER_PWM_INDEX_0, FALSE);
}
